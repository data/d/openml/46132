# OpenML dataset: NCI_60_Thioguanine

https://www.openml.org/d/46132

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This pharmacogenomic study investigates the patterns of drug activity in cancer cell lines. These cell lines come from the NCI-60 Human Tumor Cell Lines established by the Developmental Therapeutics Program of the National Cancer Institute (NCI) to screen for the toxicity of chemical compound repositories in diverse cancer cell lines. NCI-60 includes cell lines derived from cancers of colorectal (7 cell lines), renal (8), ovarian (6), breast (8), prostate (2), lung (9) and central nervous system origin (6), as well as leukemia (6) and melanoma (8).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46132) of an [OpenML dataset](https://www.openml.org/d/46132). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46132/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46132/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46132/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

